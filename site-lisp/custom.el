(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (org ace-window ox-reveal groovy-mode fish-mode monky yaml-mode racket-mode paredit markdown-mode magit js2-mode iy-go-to-char htmlize haskell-mode geiser deft cider android-mode))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(diff-added ((t (:foreground "cyan"))))
 '(diff-changed ((t (:foreground "magenta"))))
 '(diff-context ((t (:foreground "green"))))
 '(diff-file-header ((t (:background "black" :foreground "yellow" :weight ultra-bold))))
 '(diff-header ((t (:background "black" :foreground "yellow"))))
 '(diff-hunk-header ((t (:background "black" :foreground "gold"))))
 '(diff-removed ((t (:foreground "red"))))
 '(show-paren-match ((((class color) (background light)) (:background "dark slate blue"))))
 '(which-func ((t (:foreground "dodger blue")))))
